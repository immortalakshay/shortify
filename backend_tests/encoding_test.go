package main

import (
	"bitbucket.org/immortalakshay/shortify/backend/controllers"
	"testing"
)

func TestEncode(t *testing.T) {
	encoded := controllers.Encode(63)
	if encoded != "-" {
		t.Errorf("Sum was incorrect, got: %s, want: %s.", encoded, "-")
	}
}

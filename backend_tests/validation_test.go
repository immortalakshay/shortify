package main

import (
	"bitbucket.org/immortalakshay/shortify/backend/utils/validate"
	"testing"
)

func TestValidation(t *testing.T) {
	valid := validate.Url("https://www.ee.ryerson.ca/~fyuan/")
	if !valid {
		t.Errorf("Sum was incorrect, got: %t, want: %t.", valid, true)
	}
}

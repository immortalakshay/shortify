import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import {connect} from 'react-redux';
import { Button, Form, FormGroup, Input } from 'reactstrap';
import * as actionCreators from './action-creators.js';
// import { Map } from 'immutable';

class App extends Component {

    constructor(props) {
        super(props);
        console.log(this.props.dispatch);
        this.state = {
            newLongUrlValue: "",
            longUrls: {}
        }
        ;
        this.handleChange = this.handleChange.bind(this);
        this.getShortUrl = this.getShortUrl.bind(this);
    }


    handleChange(event) {
        this.setState({newLongUrlValue: event.target.value})
    }
    render() {
        console.log(this.props);
        console.log(this.state);

        let longUrlRows = [];
        // this.props.longUrls.valueSeq().forEach(_longUrl => longUrlRows.push(<tr key={_longUrl.get('id')}><td>{_longUrl.get('id')}</td><td>{_longUrl.get('value')}</td></tr>));
        for (let key in this.props.longUrls) {
            if (this.props.longUrls.hasOwnProperty(key)) {
                console.log(key);
                let _longUrl = this.props.longUrls[key];
                let shortUrl = "http://"+window.location.hostname + '/r/'+_longUrl.id;
                longUrlRows.push(<tr key={key}><td><a href={shortUrl}>{shortUrl}</a></td><td>{_longUrl.value}</td></tr>);
            }
        }
        console.log(longUrlRows);

        return (
            <div className="App">
                {/*<header className="App-header">*/}

                {/*</header>*/}
                <div className="body">
                    <Form onSubmit={this.getShortUrl}>
                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                            <Input type="url" name="url" id="exampleUrl" value={this.state.newLongUrlValue} onChange={this.handleChange} placeholder="https://www.ee.ryerson.ca/~fyuan/"  bsSize="lg"/>
                        </FormGroup>
                        <Button>Submit</Button>
                    </Form>

                    <table>
                        <thead>
                        <tr>
                            <th>Short URL</th>
                            <th>URL</th>
                        </tr>
                        </thead>
                        <tbody>
                        {longUrlRows}
                        </tbody>
                    </table>
                </div>

            </div>
    );
  }

    getShortUrl(event) {
        event.preventDefault();
        this.props.dispatch(actionCreators.shorten({value: this.state.newLongUrlValue}));
    }
}
function mapStateToProps(state) {
    console.log(state);
    return {
        longUrls: state.get('longUrls')
    }
}


export const AppContainer = connect(
    mapStateToProps
)(App);

import { Map } from 'immutable';
import objectAssign  from 'object-assign';

const initialState = Map({
    longUrls: {}
});

export default function(state = initialState, action) {
    console.log('reducing action of type '+action.type);
    switch (action.type) {
        case 'GET_SHORT_URL_RESPONSE':
            console.log(action.payload);
            let new_long_url = {};
            new_long_url[action.payload.id] = {id: action.payload.id, value: action.payload.value};
            let new_long_urls = objectAssign({}, state.get('longUrls'), new_long_url );
            return state.set('longUrls', new_long_urls);
        default:
            return state;
    }
}

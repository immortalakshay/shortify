import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducer from './reducer.js';
import { apiMiddleware } from 'redux-api-middleware';
import './index.css';
import {AppContainer} from './App';
// import * as serviceWorker from './serviceWorker';

const store = createStore(
    reducer,
    applyMiddleware(apiMiddleware)
);

ReactDOM.render(
    <Provider store={store}>
        <AppContainer />
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();

import { RSAA } from 'redux-api-middleware';

const REQUEST = '/user/REQUEST';
const RECEIVE = '/user/RECEIVE';
const FAILURE = '/user/FAILURE';

export function shorten(longUrl) {
    console.log(JSON.stringify(longUrl));
    return {
        [RSAA]: {
            endpoint: `/api/longUrl`,
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(longUrl),
            types: [
                {
                    type: 'REQUEST',
                    payload: (action, state) => ({ action: state })
                },
                'GET_SHORT_URL_RESPONSE',
                // {
                //     type: 'GET_SHORT_URL_RESPONSE',
                //     payload: (action, state, response) => {
                //         let res = response.json().then((json) => ({ 'shortUrl': json['short_url']}));
                //         console.log(res);
                //         return res;
                //     }
                // },
                'FAILURE'
            ]
        }
    }
}


// export function setClientId(properties) {
//     return {
//         type: 'SET_CLIENT_ID',
//         properties
//     };
// }
//
// export function setConnectionState(properties) {
//     return {
//         type: 'SET_CONNECTION_STATE',
//         properties
//     };
// }
//
// export function getWorkerGroups(properties) {
//     return {
//         meta: {remote: true},
//         type: 'GET_WORKER_GROUPS',
//         properties
//     };
// }
//
// export function getEntryPointStateName(properties) {
//     return {
//         meta: {remote: true},
//         type: 'GET_ENTRY_POINT_STATE_NAME',
//         properties
//     };
// }
//
// export function getStates(properties) {
//     return {
//         meta: {remote: true},
//         type: 'GET_STATES',
//         properties
//     };
// }
//
// export function getWorkflow(properties) {
//     return {
//         meta: {remote: true},
//         type: 'GET_WORKFLOW',
//         properties
//     };
// }
//
// export function getWorkers(properties) {
//     return {
//         meta: {remote: true},
//         type: 'GET_WORKERS',
//         properties
//     };
// }
//
// export function postWorkerGroups(properties) {
//     return {
//         meta: {remote: true},
//         type: 'POST_WORKER_GROUPS',
//         properties
//     };
// }
//
// export function postEntryPointStateName(properties) {
//     return {
//         meta: {remote: true},
//         type: 'POST_ENTRY_POINT_STATE_NAME',
//         properties
//     };
// }
//
// export function postStates(properties) {
//     return {
//         meta: {remote: true},
//         type: 'POST_STATES',
//         properties
//     };
// }
//
// export function patchWorkflow(properties) {
//     return {
//         meta: {remote: true},
//         type: 'PATCH_WORKFLOW',
//         properties
//     };
// }
//
// export function getWorkerGroupsResponse(properties) {
//     return {
//         type: 'GET_WORKER_GROUPS_RESPONSE',
//         properties
//     };
// }
//
// export function getEntryPointStateNameResponse(properties) {
//     return {
//         type: 'GET_ENTRY_POINT_STATE_NAME_RESPONSE',
//         properties
//     };
// }
//
// export function getStatesResponse(properties) {
//     return {
//         type: 'GET_STATES_RESPONSE',
//         properties
//     };
// }
//
// export function getWorkflowResponse(properties) {
//     return {
//         type: 'GET_WORKFLOW_RESPONSE',
//         properties
//     };
// }
//
// export function getWorkersResponse(properties) {
//     return {
//         type: 'GET_WORKERS_RESPONSE',
//         properties
//     };
// }
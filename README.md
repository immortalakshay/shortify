# shortify

DealTap Software Architect Assessment

## Overview

This is a URL shortening web application designed for high volume. It has 4 components 

1. Front End 

Front-end uses the following technologies
    
    1. React
    2. Redux
    3. redux-api-middleware

2. Back End

Back-end is written in golang and uses standard libraries

3. Caching

Caching is done using redis

4. Persistence

Data is Persisted in a Postgres Database

## Production Deployment

This application can be deployed on any cloud platform as it does not use any vendor specific services.

### AWS 

This application can be deployed using some AWS services that take care of managing services

1. FrontEnd 

AWS offers a CDN called Cloudfront that can be used to host the front end client. A CI/CD pipeline can build and push the front end client to an S3 bucket and this bucket can be set as a source for CloudFront Distribution

2. BackEnd

AWS offers various Compute solutions. EKS and Lambda being the most elastic. EKS is slightly better for this use case because

    1. Portability: Can deploy in similar way to any cloud infrastructure that has kubernetes
    2. Lifecycle Control: More control of how many container run than Lambda
    3. Smaller Deliverable: Than EC2 AMI
    
3. Caching

AWS offers a managed caching service called Elasticache that can serve a redis cache

4. Persistence 

AWS offers a managed database service called RDS that can host a Postgres Database


## Shortening Algorithm
Basically, No compression algorithm can compress a url of length `n` to a 6 character long alphanumeric string.
So, the next solution is mapping. i.e. We assign a 6 character alphanumeric to a url and store that. whenever someone wants to resolve the small url we lookup in the stored data.

There are in total 64 alphanumeric url safe characters. this mean there are 64^6 possibilities.
If you count combinations smaller than 6 characters  than you have 

64^6 + 64^5+ 64^4+ 64^3+ 64^2+ 64^1
= 69810262080

So basically we need a number between 0 to 69810262080 and we should be able to base64 encode it.

But, how do we do this in a distributed way. 

**BruteForce:** 

Use an auto increment column in a strong-consistency DBMS like postgres.

Problem with this is you get a hot zone in your system. the AutoIncrement is going to be called a lot

**Better Solution**

Use an auto increment but use it to generate first 4 or 5 characters of the string. for the second part of the string use a counter local to the application instance

This was the frequency of increment goes down since each instance going to ask for new first part after it has exhausted all 64 (or 64^2 if doing a 4-2 split) 

Issue:

One (minor) issue with this solution is that if a node goes down you loose the remaining combinations that node could have made.
This can be controlled if you tune your scaling policies 

## API 

The api documentation is done using Open-Api-Spec stored in `backend/api/shortify.yaml`

## TODO: Things I haven't found time to do

1. Fast and consistent counter 
I am currently using redis for auto increment counter. Redis is fast because its in-memory but it offers eventual consistence.
Need to find a more reliable solution
Are there any Strongly consistent in-memory databases ?

2. Testing
I need to write more tests !
Test cases
    1. positive: try to generate a shortUrl
    2. what if input has non url-safe character 
    3. what if Redis does'nt return when asking for long url
    4. introduce delay in DB call and check if response is received using the cache well before the delay.
    5. send a url with length more than our limit check that the response shows a validation error
    6. send a url that does not match a url regex 

End-to-End tests using docker-compose.


4. User statistics
The Redirect Controller needs to send forward the UserAgent data to a place where some Batch processing can pick it up from and extract statistics from it.


## Development Deployment
We use docker and docker-compose to bring up a complete solution locally

## Development Environment Setup Guide

https://boyter.org/posts/how-to-start-go-project-2018/
https://github.com/golang-standards/project-layout
https://github.com/alco/gostart

1. Install golang from https://golang.org/

2. Set $GOPATH
```
$Env:GOPATH = "C:\work"
```

3. Check out the project in C:\work\src\bitbucket.org\immortalakshay\shortify

4. Install dep
```
# Powershell Windows
[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
wget -O C:\Go\bin\dep.exe https://github.com/golang/dep/releases/download/v0.5.0/dep-windows-amd64.exe
```

```bash
go get -d
```

## Scratchpad (TODO : remove me)

```bash
docker build -t immortalakshay/shortify:latest .
docker run -it -p 8000:8000 immortalakshay/shortify:latest

docker build -t immortalakshay/shortify_cache:latest backend/cache
docker run -it -p 6379:6379 immortalakshay/shortify_cache:latest redis-server --requirepass yourpassword
docker run --rm --name redis-commander -e REDIS_HOST=cache -e REDIS_PASSWORD=yourpassword -e REDIS_PORT=6379 -e REDIS_DB=0 -it -p 8081:8081 rediscommander/redis-commander:latest


Download npm 

http://nodejs.org/

npm init react-app frontend

npm install redux react-redux redux-api-middleware --save

docker-compose up
```


## Checklist

* Security
* High Availability
  * Locally
    * Nginx
  * CDN
    *  AWS CloudFront
  * Caching
    * Redis (AWS Elasticache)
    * Cache Invalidation ?
  * Scaling
    * Cache (Elasticache)
    * DB (RDS)
    * Compute
      * Lambda = API gateway calls
      * ECS has some auto scaling 
  * Distribute
    * Availability Zone
    * Region
* Test
  * Linting
  * Unit Tests
  * API Tests
  * E2E Tests
  * Performance LargeScale tests
* CD
  * Jenkins
  * AWS Code pipeline
* FrontEnd
  * React
* Database
  * Postgres
  * Cleanup of old data ?
* Algorithm
  * Validation
  * No Conflict
  * Speed
* Compute
  * Fargate (EKS)
  * EC2
  * Lambda
    * SAM CLI for local testing

* Instrumentation and Logging
* API
  * Swagger

package routes

import (
	"bitbucket.org/immortalakshay/shortify/backend/controllers"
	"net/http"
)

func CreateRoutes(mux *http.ServeMux, longUrl *controllers.LongUrlController, redirect *controllers.RedirectController) {
	mux.HandleFunc("/api/longUrl", longUrl.Create)
	mux.HandleFunc("/api/longUrl/", longUrl.LongUrl)
	mux.HandleFunc("/r/", redirect.Redirect)
}

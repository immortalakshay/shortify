package main

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/immortalakshay/shortify/backend/controllers"
	"bitbucket.org/immortalakshay/shortify/backend/routes"
	"bitbucket.org/immortalakshay/shortify/backend/utils/caching"
	"bitbucket.org/immortalakshay/shortify/backend/utils/database"
)

func main() {
	db, err := database.Connect(os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_PASSWORD"), os.Getenv("POSTGRES_DB"), os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_PORT"))
	if err != nil {
		log.Fatal(err)
	}
	cache := &caching.Redis{
		Client: caching.Connect(os.Getenv("REDIS_ADDR"), os.Getenv("REDIS_PASSWORD"), 0),
	}

	shortenController := controllers.NewLongUrlController(db, cache)
	redirectController := controllers.NewRedirectController(db, cache)

	mux := http.NewServeMux()
	routes.CreateRoutes(mux, shortenController, redirectController)

	if err := http.ListenAndServe(":8000", mux); err != nil {
		log.Fatal(err)
	}
}
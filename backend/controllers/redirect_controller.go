package controllers

import (
	"bitbucket.org/immortalakshay/shortify/backend/models"
	"bitbucket.org/immortalakshay/shortify/backend/repositories"
	"bitbucket.org/immortalakshay/shortify/backend/responses"
	"bitbucket.org/immortalakshay/shortify/backend/utils/caching"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"time"
)

type RedirectController struct {
	DB    *sql.DB
	Cache caching.Cache
}

func NewRedirectController(db *sql.DB, c caching.Cache) *RedirectController {
	return &RedirectController{
		DB:    db,
		Cache: c,
	}
}

func (rc *RedirectController) getValueFromDB(id string, channel chan responses.GetLongUrlFromStorageResponse) {
	//time.Sleep(5 * time.Second)
	longUrl, err := repositories.GetLongUrlByID(rc.DB, id)
	if err != nil {
		log.Printf("error getting long url by id: %s", err)
		channel <- responses.GetLongUrlFromStorageResponse{LongUrl:longUrl, Successful:false, IsFromCache:false}
		return
	}
	channel <- responses.GetLongUrlFromStorageResponse{LongUrl:longUrl, Successful:true, IsFromCache:false}
}

func (rc *RedirectController) getValueFromCache(id string, channel chan responses.GetLongUrlFromStorageResponse) {
	longUrl := models.LongUrl{Id:id}
	value, err := rc.Cache.Get(id)

	if err != nil {
		log.Printf("not in cache: %s", err)
		channel <- responses.GetLongUrlFromStorageResponse{LongUrl:&longUrl, Successful:false, IsFromCache:true}
		return
	}
	longUrl.Value = value
	channel <- responses.GetLongUrlFromStorageResponse{LongUrl:&longUrl, Successful:true, IsFromCache:true}
}

func (rc *RedirectController) fastestGetAndIncrementHit(longUrl models.LongUrl, responseChannel chan responses.FastestGetResponse) {
	channel := make(chan responses.GetLongUrlFromStorageResponse)
	go rc.getValueFromCache(longUrl.Id, channel)
	go rc.getValueFromDB(longUrl.Id, channel)
	needToCache := true
	sent := false
	getResponse := <- channel
	if getResponse.Successful {
		longUrl.Value = getResponse.LongUrl.Value
		responseChannel <- responses.FastestGetResponse{LongUrl:&longUrl}
		sent = true
		if getResponse.IsFromCache {
			needToCache = false
		}
	}

	getResponse = <- channel
	if getResponse.Successful {
		if !sent {
			longUrl.Value = getResponse.LongUrl.Value
			responseChannel <- responses.FastestGetResponse{LongUrl:&longUrl}
			sent = true
		}
		if getResponse.IsFromCache {
			needToCache = false
		}

	}

	if needToCache{
		log.Printf("caching")
		rc.Cache.Set(longUrl.Value, longUrl.Id, time.Duration(100 * time.Second))
		rc.Cache.Set(longUrl.Id, longUrl.Value, time.Duration(100 * time.Second))
	}

	log.Printf("incrementing")
	err := repositories.IncrementHitByID(rc.DB, longUrl.Id)
	if err != nil {
		responseChannel <- responses.FastestGetResponse{Error:err, Code:http.StatusInternalServerError}
		return
	}
}

func (rc *RedirectController) Redirect(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	if r.URL.Path == "/favicon.ico" {
		return
	}
	log.Println("path is")
	log.Println(r.URL.Path)
	re := regexp.MustCompile("/r/([0-9a-zA-Z_\\-]+)")
	fmt.Println(r.URL.Path)
	ids := re.FindStringSubmatch(r.URL.Path)
	if len(ids) < 2 {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	id := ids[1]
	log.Printf("got id")
	log.Printf(id)
	fmt.Println(id)

	longUrl := models.LongUrl{Id:id}

	if r.Method == "GET" {

		w.Header().Set("Content-Type", "application/json")
		responseChannel  := make(chan responses.FastestGetResponse)
		log.Printf("starting goroutine")
		go rc.fastestGetAndIncrementHit(longUrl, responseChannel)
		log.Printf("reading from goroutine")
		response := <- responseChannel
		log.Printf("read from goroutine")
		if response.Error != nil{
			http.Error(w, "Not Found", response.Code)
			return
		}
		log.Printf("readirecting to " + response.LongUrl.Value)
		http.Redirect(w, r, response.LongUrl.Value, 301)
		return
	}
}

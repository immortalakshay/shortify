package controllers

import (
	"bitbucket.org/immortalakshay/shortify/backend/models"
	"bitbucket.org/immortalakshay/shortify/backend/repositories"
	"bitbucket.org/immortalakshay/shortify/backend/requests"
	"bitbucket.org/immortalakshay/shortify/backend/responses"
	"bitbucket.org/immortalakshay/shortify/backend/utils/caching"
	"bitbucket.org/immortalakshay/shortify/backend/utils/validate"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"regexp"
	"sync"
	"time"
)


const (
	alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-"
	base     = int64(len(alphabet))
	cacheExpiration = time.Duration(100 * time.Second)
)

// Encode decoded integer to base64 string.
//func Encode(n int64) string {
//	b := make([]byte, 0, 512)
//	b = append([]byte{alphabet[int(n)]}, b...)
//	return string(b)
//}

//Encode decoded integer to base62 string.
func Encode(n int64) string {
	if n == 0 {
		return "0"
	}

	b := make([]byte, 0, 512)
	for n > 0 {
		r := math.Mod(float64(n), float64(base))
		n /= base
		b = append([]byte{alphabet[int(r)]}, b...)
	}
	return string(b)
}

type LongUrlController struct {
	DB    *sql.DB
	Cache caching.Cache
	UrlA string
	Counter int64
	mux sync.Mutex
}

func NewLongUrlController(db *sql.DB, c caching.Cache) *LongUrlController {
	return &LongUrlController{
		DB:    db,
		Cache: c,
		Counter: 0,
	}
}

func (lc *LongUrlController) getValueFromDB(id string, channel chan responses.GetLongUrlFromStorageResponse) {
	//time.Sleep(6 * time.Second)
	longUrl, err := repositories.GetLongUrlByID(lc.DB, id)
	if err != nil {
		log.Printf("error getting long url by id: %s", err)
		channel <- responses.GetLongUrlFromStorageResponse{LongUrl:longUrl, Successful:false, IsFromCache:false}
		return
	}
	channel <- responses.GetLongUrlFromStorageResponse{LongUrl:longUrl, Successful:true, IsFromCache:false}
}
func (lc *LongUrlController) getIdFromDB(value string, channel chan responses.GetLongUrlFromStorageResponse) {
	//time.Sleep(6 * time.Second)
	longUrl, err := repositories.GetLongUrlByValue(lc.DB, value)
	if err != nil {
		log.Printf("error getting long url by value: %s", err)
		channel <- responses.GetLongUrlFromStorageResponse{LongUrl:longUrl, Successful:false, IsFromCache:false}
		return
	}
	channel <- responses.GetLongUrlFromStorageResponse{LongUrl:longUrl, Successful:true, IsFromCache:false}
}

func (lc *LongUrlController) getValueFromCache(id string, channel chan responses.GetLongUrlFromStorageResponse) {
	longUrl := models.LongUrl{Id:id}
	value, err := lc.Cache.Get(id)

	if err != nil {
		log.Printf("id not in cache: %s", err)
		channel <- responses.GetLongUrlFromStorageResponse{LongUrl:&longUrl, Successful:false, IsFromCache:true}
		return
	}
	longUrl.Value = value
	channel <- responses.GetLongUrlFromStorageResponse{LongUrl:&longUrl, Successful:true, IsFromCache:true}
}

func (lc *LongUrlController) getIdFromCache(value string, channel chan responses.GetLongUrlFromStorageResponse) {
	longUrl := models.LongUrl{Value:value}
	id, err := lc.Cache.Get(value)
	if err != nil {
		log.Printf("value not in cache: %s", err)
		channel <- responses.GetLongUrlFromStorageResponse{LongUrl:&longUrl, Successful:false, IsFromCache:true}
		return
	}
	longUrl.Id = id
	channel <- responses.GetLongUrlFromStorageResponse{LongUrl:&longUrl, Successful:true, IsFromCache:true}
}

func (lc *LongUrlController) generateIdA() {
	result, err := lc.Cache.Increment("counter")
	if err != nil {
		log.Fatalf("could not increment: %s", err)
	}
	lc.UrlA = Encode(result)
}

func (lc *LongUrlController) generateId(longUrl string) string {
	lc.mux.Lock()
	needNewUrlA := false
	if lc.Counter == base -1 {
		needNewUrlA = true
		lc.Counter = -1
	}
	if lc.UrlA == ""{
		needNewUrlA = true
	}
	lc.Counter ++
	if needNewUrlA {
		lc.generateIdA()
	}
	url := lc.UrlA + Encode(lc.Counter)
	lc.mux.Unlock()
	return url
}



func (lc *LongUrlController) fastestGetOrCreate(longUrl models.LongUrl, responseChannel chan responses.FastestGetResponse) {
	channel := make(chan responses.GetLongUrlFromStorageResponse)
	go lc.getIdFromCache(longUrl.Value, channel)
	go lc.getIdFromDB(longUrl.Value, channel)
	getResponse := <- channel
	needToCache := true
	needToPersist := false
	sent := false
	responseLongUrl := models.LongUrl{Value:longUrl.Value}
	if getResponse.Successful {
		log.Printf("got first response")
		responseLongUrl.Id = getResponse.LongUrl.Id
		responseChannel <- responses.FastestGetResponse{LongUrl:&responseLongUrl}
		sent = true
		if getResponse.IsFromCache {
			needToCache = false
		}
	}
	getResponse = <- channel
	if getResponse.Successful {
		log.Printf("got second response")
		responseLongUrl.Id = getResponse.LongUrl.Id
		if !sent {
			responseChannel <- responses.FastestGetResponse{LongUrl:&responseLongUrl}
			sent = true
		}
		if getResponse.IsFromCache {
			needToCache = false
		}
	}

	if !sent {
		log.Printf("generating")
		id := lc.generateId(longUrl.Value)
		responseLongUrl.Id = id
		responseChannel <- responses.FastestGetResponse{LongUrl:&responseLongUrl}
		needToPersist = true
	}

	if needToCache{
		log.Printf("caching")
		lc.Cache.Set(responseLongUrl.Value, responseLongUrl.Id, cacheExpiration)
		lc.Cache.Set(responseLongUrl.Id, responseLongUrl.Value, cacheExpiration)
	}


	if needToPersist{
		log.Printf("persisting")
		err := repositories.CreateLongUrl(lc.DB, responseLongUrl.Id, responseLongUrl.Value)
		if err != nil {
			responseChannel <- responses.FastestGetResponse{Error:err, Code:http.StatusInternalServerError}
			return
		}
	}
}


func (lc *LongUrlController) Create(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	decoder := json.NewDecoder(r.Body)
	var nlur requests.NewLongUrlRequest
	err := decoder.Decode(&nlur)
	if err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}

	valid := validate.Url(nlur.Value)
	if ! valid {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}

	longUrl := models.LongUrl{Value:nlur.Value}

	if r.Method == "POST" {
		w.Header().Set("Content-Type", "application/json")
		responseChannel  := make(chan responses.FastestGetResponse)
		go lc.fastestGetOrCreate(longUrl, responseChannel)
		response := <- responseChannel
		if response.Error != nil{
			http.Error(w, "Not Found", response.Code)
			return
		}
		json.NewEncoder(w).Encode(response.LongUrl)
		return
	}
}

func (lc *LongUrlController) fastestGet(longUrl models.LongUrl, responseChannel chan responses.FastestGetResponse) {
	channel := make(chan responses.GetLongUrlFromStorageResponse)
	go lc.getValueFromCache(longUrl.Id, channel)
	go lc.getValueFromDB(longUrl.Id, channel)
	needToCache := true
	getResponse := <- channel
	if getResponse.Successful {
		longUrl.Value = getResponse.LongUrl.Value
		longUrl.Hits = getResponse.LongUrl.Hits
		if getResponse.IsFromCache {
			needToCache = false
		} else {
			responseChannel <- responses.FastestGetResponse{LongUrl:&longUrl}
		}
	}

	getResponse = <- channel
	if getResponse.Successful {
		longUrl.Value = getResponse.LongUrl.Value
		longUrl.Hits = getResponse.LongUrl.Hits
		if getResponse.IsFromCache {
			needToCache = false
		} else{
			responseChannel <- responses.FastestGetResponse{LongUrl:&longUrl}
		}

	}

	if needToCache{
		log.Printf("caching")
		lc.Cache.Set(longUrl.Value, longUrl.Id, cacheExpiration)
		lc.Cache.Set(longUrl.Id, longUrl.Value, cacheExpiration)
	}
}

func (lc *LongUrlController) LongUrl(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	if r.URL.Path == "/favicon.ico" {
		return
	}
	re := regexp.MustCompile("/api/longUrl/([0-9a-zA-Z_\\-]+)")
	fmt.Println(r.URL.Path)
	ids := re.FindStringSubmatch(r.URL.Path)
	id := ids[1]
	fmt.Println(id)

	longUrl := models.LongUrl{Id:id}

	if r.Method == "GET" {
		w.Header().Set("Content-Type", "application/json")
		responseChannel  := make(chan responses.FastestGetResponse)
		go lc.fastestGet(longUrl, responseChannel)
		response := <- responseChannel
		if response.Error != nil{
			http.Error(w, "Not Found", response.Code)
			return
		}
		json.NewEncoder(w).Encode(response.LongUrl)
		return
	}
}

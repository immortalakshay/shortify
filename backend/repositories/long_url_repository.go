package repositories

import (
	"bitbucket.org/immortalakshay/shortify/backend/models"
	"database/sql"
)

func GetLongUrlByID(db *sql.DB, id string) (*models.LongUrl, error) {
	const query = `
		select
			id,
			value,
            hits
		from
			long_url
		where
			id = $1
	`
	var longUrl models.LongUrl
	err := db.QueryRow(query, id).Scan(&longUrl.Id, &longUrl.Value, &longUrl.Hits)
	return &longUrl, err
}

func GetLongUrlByValue(db *sql.DB, value string) (*models.LongUrl, error) {
	const query = `
		select
			id,
			value
		from
			long_url
		where
			value = $1
	`
	var longUrl models.LongUrl
	err := db.QueryRow(query, value).Scan(&longUrl.Id, &longUrl.Value)
	return &longUrl, err
}

func IncrementHitByID(db *sql.DB, id string) error {
	const query = `
        update
            long_url
		set
            hits = hits + 1
		where
			id = $1
	`
	_, err := db.Exec(query, id)
	return err
}

func CreateLongUrl(db *sql.DB, id string, value string) error {
	const query = `
		insert into long_url (
			id,
			value
		) values (
			$1,
			$2
		)
	`
	_, err := db.Exec(query, id, value)
	return err
}

package responses

import "bitbucket.org/immortalakshay/shortify/backend/models"

type GetLongUrlFromStorageResponse struct {
	LongUrl         *models.LongUrl
	Successful      bool
	IsFromCache     bool
}
type FastestGetResponse struct {
	LongUrl         *models.LongUrl
	Error           error
	Code            int
}

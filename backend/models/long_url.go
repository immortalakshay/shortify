package models

type LongUrl struct {
	Value    string `json:"value"`
	Id       string `json:"id"`
	Hits     int64 `json:"hits"`
}

package validate

import (
	"regexp"
)

func Url(url string) bool {
	if len(url) > 2048{
		return false
	}
	Re := regexp.MustCompile(`^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:%\/?#[\]@!\$&'\(\)\*\+,;=.]+$`)
	return Re.MatchString(url)
}

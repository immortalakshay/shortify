package requests

type NewLongUrlRequest struct {
	Value    string `json:"value"`
}

create table long_url (
    id varchar(60) primary key,
    value varchar(150) not null,
    hits int not null default 0
);

